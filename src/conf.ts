import yargs from 'yargs';
import { Args } from './common/types';

const DEFAULT_MAX_FILES = 1;
const DEFAULT_CACHE_SIZE = 209715200; // 200 MB

const argv = yargs
  .number('s')
  .boolean('r')
  .string('l')
  .string('j')
  .number('v')
  .boolean('p')
  .number('max-files')
  .number('cache-size').argv;

const args: Args = {
  sensivity: parseFloat(String(argv.s)) || 0.2,
  recursive: argv.r || false,
  log: argv.l || null,
  paths: argv._,
  json: argv.j || null,
  maxFiles: parseInt(String(argv['max-files'])) || DEFAULT_MAX_FILES,
  cacheSize: parseInt(String(argv['cache-size'])) || DEFAULT_CACHE_SIZE,
  verbose: parseInt(String(argv.v)) || 2,
  usePixel: Boolean(argv.p),
};

export { args, Args, DEFAULT_MAX_FILES, DEFAULT_CACHE_SIZE };
