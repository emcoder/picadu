import log4js from 'log4js';
import { args } from '../conf';

type LogLevel = 'trace' | 'debug' | 'info' | 'warn' | 'error';

function getLevel(verbose: number): LogLevel {
  const levels = ['trace', 'debug', 'info', 'warn', 'error'].reverse();
  const level: LogLevel = levels[verbose] as LogLevel;
  if (!level) {
    throw new Error(`Unsupported verbosity level: ${verbose}`);
  }
  return level;
}

function getLogger() {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const appenders: any = {
    stdout: { type: 'stdout' },
  };

  if (args.log) {
    appenders.file = { type: 'file', filename: args.log };
  }

  log4js.configure({
    appenders,
    categories: {
      default: {
        appenders: Object.keys(appenders),
        level: getLevel(args.verbose) || 'info',
      },
    },
  });

  return log4js.getLogger();
}

export default getLogger();
