import { exec } from 'child_process';
import { readFile } from 'fs';

type JsonResult = [string, { path: string }[]][];

function runCmd(cmd: string): Promise<[string, string]> {
  return new Promise((resolve, reject): void => {
    exec(cmd, (err, stdout, stderr) => {
      if (err) {
        reject(err);
      }
      resolve([stdout, stderr]);
    });
  });
}

async function readJsonResult(file: string): Promise<JsonResult> {
  return new Promise((resolve, reject): void => {
    readFile(file, 'utf-8', (err, data) => {
      if (err) {
        reject(err);
      }

      resolve(JSON.parse(data));
    });
  });
}

export { JsonResult, runCmd, readJsonResult };
