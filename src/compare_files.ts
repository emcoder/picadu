import isImage from 'is-image';

import { args } from './conf';
import { FilePath } from './common/types';
import getHash from './common/get_hash';
import getDistance from './common/get_distance';

async function compareFiles(path1: FilePath, path2: FilePath) {
  if (!isImage(path1.toString())) {
    throw new Error(`Not supported image file: ${path1}`);
  }

  if (!isImage(path2.toString())) {
    throw new Error(`Not supported image file: ${path2}`);
  }

  const [hash1, hash2] = await Promise.all([getHash(path1), getHash(path2)]);

  const distance = getDistance(hash1, hash2);
  const percDistance = Math.round((1 - distance) * 100);
  const roundDistance = distance.toFixed(2);

  if (distance <= args.sensivity) {
    console.info(`[${percDistance}% | ${roundDistance}]`, 'Probably, duplicate images :)');
  } else {
    console.info(`[${percDistance}% | ${roundDistance}]`, 'Probably, different images :(');
  }
}

export default compareFiles;
