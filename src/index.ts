import { isDirectory } from './utils';
import { args } from './conf';
import log from './common/log';
import singleDir from './single_dir';
import compareFiles from './compare_files';

async function run() {
  const pathsCount = args.paths.length;

  if (!pathsCount) {
    throw new Error('No path arguments provided');
  }

  if (pathsCount === 1) {
    await singleDir(args.paths[0], args.recursive);
  } else if (pathsCount === 2) {
    const [path1, path2] = args.paths;

    const [isDir1, isDir2] = await Promise.all([isDirectory(path1), isDirectory(path2)]);

    // Both paths are files, just compare
    if (!isDir1 && !isDir2) {
      await compareFiles(path1, path2);
    } else if (!isDir1 && isDir2) {
      // TODO: Find duplicates in some directory for one image
    } else {
      throw new Error('Unsupported mode');
    }
  } else {
    // TODO: Three or more paths — search between all of them, like in single dir mode
    log.warn('Unimplemented yet');
  }
}

async function main() {
  await run();
}

main()
  .then(() => log.info('Success'))
  .catch(err => log.error(err));

export default main;
