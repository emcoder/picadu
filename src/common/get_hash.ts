import Jimp from 'jimp';

import { ImagePath, Hash } from './types';

export default async function getHash(file: ImagePath): Promise<Hash> {
  const image = await Jimp.read(file);
  // eslint-disable-next-line
  // @ts-ignore
  const hash = image.pHash();
  return hash;
}
