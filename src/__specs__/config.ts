import path from 'path';

const rootPath = path.resolve(path.join(__dirname, '..', '..'));
const appCmd = `node ${rootPath}/dist/index.js`;
const jsonFile = `${__dirname}/result.json`;
const dataDir = `${__dirname}/data/`;

const validResult = new Map([
  ['A.jpg', ['A1.jpg', 'sub/A2.png']],
  ['A1.jpg', ['A.jpg', 'sub/A2.png']],
  ['sub/A2.png', ['A.jpg', 'A1.jpg']],
  ['B.jpg', ['B1.jpg', 'sub/B2.gif']],
  ['B1.jpg', ['B.jpg', 'sub/B2.gif']],
  ['sub/B2.gif', ['B.jpg', 'B1.jpg']],
]);

export { rootPath, validResult, appCmd, jsonFile, dataDir };
