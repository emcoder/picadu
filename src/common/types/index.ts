interface Args {
  sensivity: number;
  recursive: boolean;
  log: FilePath | null;
  paths: FilePath[];
  json: FilePath | null;
  maxFiles: number;
  cacheSize: number;
  verbose: number;
  usePixel: boolean;
}

type Hash = string;

type Path = string;
type DirPath = Path;
type FilePath = Path;
type ImagePath = FilePath;

type Distance = number;

interface Dupe {
  path: ImagePath;
  distance: Distance;
}

export { Args, Hash, Path, DirPath, FilePath, ImagePath, Dupe, Distance };
