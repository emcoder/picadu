import { Hash, ImagePath } from './types';
import { arrayChunks } from '../utils';
import { DEFAULT_MAX_FILES } from '../conf';
import log from './log';
import getHash from './get_hash';

interface CollectParams {
  maxFiles: number;
}

class Hashes {
  readonly hashes: Map<ImagePath, Hash> = new Map();

  async collect(
    files: ImagePath[],
    params: CollectParams = {
      maxFiles: DEFAULT_MAX_FILES,
    },
  ) {
    const { maxFiles } = params;

    const chunks: ImagePath[][] = arrayChunks(files, maxFiles);
    let counter = 0;

    for (const chunk of chunks) {
      const promises = chunk.map(async (file: ImagePath) => {
        const hash = await getHash(file);
        this.hashes.set(file, hash);
        log.debug(`Save hash for #${counter++} ${file}: ${hash}`);
      });
      await Promise.all(promises);
    }
  }

  get(file: ImagePath): Hash {
    const hash = this.hashes.get(file);
    if (!hash) {
      throw new Error(`Failed to query hash for ${file}`);
    }

    return hash;
  }
}

export default Hashes;
