import { runCmd, readJsonResult } from './utils';
import { appCmd, dataDir, validResult } from './config';
import tmp from 'tmp';

it('Processess recursively single directory', async () => {
  const jsonFile = tmp.fileSync({ postfix: '.json' });
  const cmd = `${appCmd} -r -s 0.4 -j "${jsonFile.name}" "${dataDir}"`;
  await runCmd(cmd);

  const result = await readJsonResult(jsonFile.name);

  result.forEach(entry => {
    const image = entry[0].replace(dataDir, '');
    const copies = entry[1].map(copy => copy.path.replace(dataDir, '')).sort();

    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    const validCopies = validResult.get(image)!.sort();

    expect(validCopies).toBeTruthy();
    expect(copies.length).toEqual(validCopies.length);

    copies.forEach((copy, index) => expect(copy).toEqual(validCopies[index]));
  });

  jsonFile.removeCallback();
});
