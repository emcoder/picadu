import Jimp from 'jimp';
import { Hash, Distance } from './types';

export default function getDistance(a: Hash, b: Hash): Distance {
  return Jimp.compareHashes(a, b);
}
