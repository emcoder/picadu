jest.mock('jimp', () => ({
  read: (path: string): object => ({
    pHash: (): string => `hash-${path}`,
  }),
}));

import Hashes from '../hashes';

it('Collects image hashes for files', async () => {
  const hashes = new Hashes();
  await hashes.collect(['a.png', 'b.jpg']);

  expect([...hashes.hashes]).toEqual([['a.png', 'hash-a.png'], ['b.jpg', 'hash-b.jpg']]);
});
