import Hashes from './hashes';
import Dupes from './dupes';

/**
 * Application and search state
 */
class State {
  readonly hashes: Hashes;
  readonly dupes: Dupes;

  constructor() {
    this.hashes = new Hashes();
    this.dupes = new Dupes();
  }
}

const state = new State();

export default state;
