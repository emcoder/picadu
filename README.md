# Picadu
**Picadu** — image duplicates by image hash search tool, implemented with Node.js, TypeScript and Jimp library.

Example usage:
```bash
npm build
# Recursively search in directory and try to find duplicates for each image
npm start /path/to/directory/ -r -s 0.2
# Just compare two images with default sensitivity 0.15
npm start image1.png image2.jpg
```
