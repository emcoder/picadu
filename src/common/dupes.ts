import { Dupe, ImagePath } from './types';
import log from './log';

class Dupes {
  private readonly dupes: Map<ImagePath, Dupe[]> = new Map();

  add(image: ImagePath, dupe: ImagePath, distance: number) {
    let dupes = this.dupes.get(image);

    if (dupes) {
      const exists = dupes.find(d => d.path === dupe);
      if (exists) {
        return;
      }
    } else {
      dupes = [];
      this.dupes.set(image, dupes);
    }

    dupes.push({
      path: dupe,
      distance,
    });
  }

  has(image: ImagePath, compareImage: ImagePath): boolean {
    const dupes = this.dupes.get(image);
    if (!dupes) {
      return false;
    }

    const dupe = dupes.find(d => d.path === compareImage);
    return Boolean(dupe);
  }

  print(): void {
    this.dupes.forEach((dupes, file) => {
      if (dupes.length > 0) {
        this.printFile(file, dupes);
      }
    });
  }

  printFile(file: ImagePath, dupes: Dupe[]) {
    log.info(`${file}:`);
    dupes.forEach(dupe => {
      const { path: dupePath, distance } = dupe;

      log.info(` - ${dupePath} (${distance})`);
    });
  }

  toJson(): string {
    return JSON.stringify([...this.dupes], undefined, 2);
  }
}

export default Dupes;
