import isImage from 'is-image';
import { checkDir, collectFiles, writeFile } from './utils';
import { args } from './conf';
import { DirPath, Path, ImagePath } from './common/types';
import log from './common/log';
import state from './common/state';
import getDistance from './common/get_distance';

function searchDupes(file: ImagePath, files: ImagePath[]) {
  const hash = state.hashes.get(file);

  files.forEach(compareFile => {
    log.debug(`Compare ${file} with ${compareFile}...`);

    if (compareFile === file) {
      return;
    }

    if (state.dupes.has(file, compareFile)) {
      return;
    }

    const compareHash = state.hashes.get(compareFile);
    const distance = getDistance(hash, compareHash);

    if (distance <= args.sensivity) {
      log.info(`Dupe ${compareFile} of ${file} found (distance ${distance.toFixed(4)})`);
      state.dupes.add(file, compareFile, distance);
      state.dupes.add(compareFile, file, distance);
    } else {
      log.debug(`File ${compareFile} not detected as dupe of ${file} (distance ${distance.toFixed(4)})`);
    }
  });
}

async function getFiles(dirPath: DirPath, recursive = false): Promise<ImagePath[]> {
  const dirFiles = await collectFiles(dirPath, recursive);
  // Keep only image files
  return dirFiles.filter(file => isImage(file.toString()));
}

async function searchDir(dirPath: DirPath, recursive = false) {
  const { hashes, dupes } = state;

  const files = await getFiles(dirPath, recursive);

  log.info(`Found ${files.length} images`);

  log.info(`Build image hashes table...`);
  await hashes.collect(files, {
    maxFiles: args.maxFiles,
  });

  files.forEach((file: ImagePath) => {
    searchDupes(file, files);
  });

  log.info('Search completed!');

  dupes.print();

  if (args.json) {
    await writeFile(args.json, dupes.toJson());
  }
}

/**
 * Search in single directory
 */
async function singleDir(dirPath: Path, recursive = false) {
  if (args.usePixel) {
    log.warn('Using pixel diff, processing can be slow!');
  }

  await checkDir(dirPath);
  await searchDir(dirPath, recursive);
}

export default singleDir;
