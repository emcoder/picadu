import fs, { PathLike } from 'fs';
import path from 'path';
import { promisify } from 'util';
import { Path, DirPath } from '../common/types';

const writeFile = promisify(fs.writeFile);

function isExists(filePath: PathLike): Promise<boolean> {
  return new Promise(resolve => {
    fs.exists(filePath, resolve);
  });
}

function isDirectory(dirPath: PathLike): Promise<boolean> {
  return new Promise((resolve, reject) => {
    isExists(dirPath).then(exists => {
      if (!exists) {
        reject(new Error(`Path ${dirPath} not exists!`));
      }

      fs.stat(dirPath, (err, stats) => {
        if (err) {
          reject(new Error(`Failed to check ${dirPath} stats: ${err.toString()}`));
        }

        resolve(stats.isDirectory());
      });
    });
  });
}

function isFile(filePath: PathLike): Promise<boolean> {
  return new Promise((resolve, reject) => {
    isExists(filePath).then(exists => {
      if (!exists) {
        reject(new Error(`Path ${filePath} not exists!`));
      }

      fs.stat(filePath, (err, stats) => {
        if (err) {
          reject(new Error(`Failed to check ${filePath} stats: ${err.toString()}`));
        }

        resolve(stats.isFile());
      });
    });
  });
}

async function checkDir(dirPath: PathLike): Promise<void> {
  const result = await isDirectory(dirPath);

  if (!result) {
    throw new Error(`Path ${dirPath} is not a directory!`);
  }
}

function readDir(dirPath: PathLike): Promise<string[]> {
  return new Promise((resolve, reject) => {
    fs.readdir(dirPath, (err, files: string[]) => {
      if (err) {
        reject(err);
      }
      resolve(files.map(f => path.join(dirPath.toString(), f)));
    });
  });
}

async function collectFiles(dirPath: DirPath, recursive = false): Promise<Path[]> {
  const result: Path[] = [];
  const files = await readDir(dirPath);

  const promises = files.map(async (file: PathLike) => {
    const isDir = await isDirectory(file);
    if (isDir) {
      if (!recursive) {
        return;
      }
      const newFiles = await collectFiles(file.toString(), recursive);
      result.push(...newFiles);
    } else {
      result.push(file.toString());
    }
  });

  await Promise.all(promises);
  return result;
}

function arrayChunks<T>(array: T[], chunkSize: number): (T[])[] {
  const chunks: (T[])[] = [];
  for (let i = 0; i < array.length; i += chunkSize) {
    chunks.push(array.slice(i, i + chunkSize));
  }
  return chunks;
}

function splitNumber(n: number): [number, number] {
  let result: [number, number] | null = null;
  for (let i = 1; i < Math.floor(Math.sqrt(n) + 1); i++) {
    if (!(n % i)) result = [i, Math.floor(n / i)];
  }
  return result || [1, 1];
}

export { isExists, checkDir, isDirectory, isFile, readDir, collectFiles, arrayChunks, splitNumber, writeFile };
